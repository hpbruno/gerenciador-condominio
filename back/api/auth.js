/*
 - @Author: Bruno Henrique
*/
require('dotenv/config')
const authSecret = process.env.AUTH_SECRET
const bcrypt = require('bcrypt-nodejs')
const jwt = require('jwt-simple')

module.exports = app => {

    const validarlogin = (req, res) => {
        
        // Vamos varer os usuários do banco, e verificar se o informado já existe. 
        const SQL = 'SELECT * FROM user US '+
                    'INNER JOIN _auth AT ON AT.EMAIL = US.EMAIL ' +
                    'WHERE US.EMAIL = ?;'        

        let values =  req.body.email 
        

        try {
            // connection 
            const con = app.config.db.connection 

            con.query(SQL, [values], (err, result, fields) =>{

                let users = result


                // Se tivermos registro, é porque o usuário está cadastrado
                if ((users) && (users.length > 0 )) {

                    users.forEach(usr => {
                        
                        if ( req.body.password != '') {
                            
                            const ismatch = bcrypt.compareSync(req.body.password, usr.PASSWORD)

                            // Verificar Hash da senha                                                        
                            if ( ismatch ) {  

                                // vamos gerar o payload do usuário para mais tarde gerar o token                     
                                const payload = {
                                    id: usr.ID, 
                                    email: usr.EMAIL,  
                                    nome: usr.NOME,  
                                    password: usr.PASSWORD, 
                                    nivel: usr.NIVEL, 
                                    iat: usr.IAT,
                                    exp: usr.EXP, 
                                }                            
                            
                                validateToken( { ...payload, token: jwt.encode(payload, authSecret) })
                                    .then(valid => {                                
                                        try {                                                         
                                            if (valid) {
                                                res.status(200).json({
                                                    ...payload, 
                                                    token:jwt.encode( payload, authSecret )                                            
                                                })
                                                return true
                                            } else {
                                                res.status(201).send('O Token expirou, entre em contato com o suporte! ')
                                                return false
                                            }
                                        } catch(err) {
                                            res.status(401).send('Falha ao verificar token!'+ err)
                                            return false
                                        }
                                    })

                            } else {

                                console.log(ismatch);
                                res.status(201).send('Senha incorreta, tente novamente!')
                                return false
                            }      
                        }      
                    });
                
                } else {            
                    res.status(201).send('Usuário não encontrado!')
                    return false
                }
                
            })   
        } catch(err) {
            res.status(501).send('Tivemos um problema para conectar com o servidor. Aguarde um moemento!')   
        }
         
        // Res 
        //res.status(200).send('OK')
    }

    const validateToken = async ( auth ) => {
        
        let isValid  

        try {
            // vamos recuperar o token
            const token = jwt.decode( auth.token, authSecret ) 

            // -- Aqui vamos fazer as validações de acesso 
            // Validade token  
            if (new Date(token.exp * 1000) > new Date() ) {
                isValid = true
            } else {
                isValid = false
            }

        } catch(err) {
            isValid = false
        }        

        return isValid
    }

    const encrypt = async ( value ) => {

        const salt = bcrypt.genSalt(10, function(err) {
            if (err) console.log(err);
        })

        return bcrypt.hashSync(value, salt)
    }


    const gerarToken = async ( user ) => {
        
        // connection  
        const con = app.config.db.connection
        
        // em segundos
        let now = Math.floor(new Date() / 1000)

        // token 
        let tkn = user.email + user.password

        // Aqui vamos criptografar 
        let email = user.email 
        let token = await encrypt(tkn)
        let iat   = now 
        let exp   = now + (60 * 60 * 24 * 30) // Melhora essa rotina

        const values = [
            [email, token, iat, exp]
        ]

        // sql 
        const sql = 'INSERT INTO _auth ' +
                    '   (EMAIL, TOKEN, IAT, EXP)' + 
                    ' VALUES ' +
                    '   (?)'
        
        con.query(sql, values, (err, res, fields) => {
            if (err) console.log('Erro ao gerar token: '+err);
        })
    }

    const registrar = async ( user ) => {
        

        if (user) {
        
            // dados para registro 
            let email = user.email 
            let password = user.password 
            
            // Vamos criptografar a senha para jogar no banco.  
            password = await encrypt(password)

            // Connection 
            const con = app.config.db.connection

                const sql = 'INSERT INTO user '+ 
                            '   (EMAIL, NOME, PASSWORD, NIVEL)' + 
                            'VALUES'+ 
                            '   (?)'

                let values = [
                  email, "teste", password, 1 
                ]

                // Criar
                con.query(sql, [values], (err, result, fields) => {
                    if (err) console.log('Falha ao criar usuário!')
                    gerarToken( user )
                    console.log('Usuário: ' + email + ' criado com sucesso!')                
                })
        }
            
    }

    const existeLogin = async (req, res) => {

        const con = app.config.db.connection
        const sql = 'SELECT COUNT(*) AS EXISTE FROM user WHERE EMAIL = ?'  
        
        let user = {
            email: req.body.email, 
            password: req.body.password           
        }
        
        if (user.email) {
            try {
            
                con.query(sql, [ user.email ], (err, result, field) =>  {
                    if (err) console.log('Erro ao cadastrar usuário');

                    let existe = result 
                    existe.forEach(ext => {
                      
                        if (ext.EXISTE > 0) {
                            res.status(201).send('Usuário ja cadastrado!')
                        } else {
                            registrar(user).then(
                            res.status(200).send('Usuário cadastrado com sucesso!'))
                        }
                    })                                                        
                })

            } catch(err) {

                console.log('Falha ao se conectar com o banco '+err);
                
            }            
        }                
    }

    return { validarlogin, gerarToken, existeLogin }
}