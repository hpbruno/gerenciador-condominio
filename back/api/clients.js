/*
 * @Author: Bruno Henrique
 */

/* Imports */
module.exports = app => {

    // Rotas de Entrada...  
    const addCliente = async (req, res) => {

        // corpo da requisicao
        let client = req.body

        // verificar se os dados para cadastro sao validos 
        let isValid = validarCliente(client)

        if (isValid === true) {
            await Cadastrar( client ) 
            res.status(201).send({ msg:'Cadastrado com sucesso!', type: 'ok'})
        } else {
            res.status(201).send(isValid)
        } 
        
    }

    const deleteCliente = async (req, res) => {

        let idCliente = req.body.idCliente
    
        // SQL
        let sql = 'DELETE C FROM clientes C WHERE C.ID = (?)'
        
        // Connection 
        const con = app.config.db.connection
        
        con.query(sql, [ idCliente ], (err, result, fields) => {
            if (err) console.log('Erro ao deletar cliente' +err);
            res.status(201).send({msg: 'Cliente removido com sucesso!', type:'ok '})
        })
    }

    const updateCliente = async (req, res) => {

        // sql 
        let upd = app.config.dic.gerarUpdate

        // body
        let cliente = req.body

        // connection 
        const con = app.config.db.connection
        
        try {
            // retornar o Update de acordo com o dic. da base. 
            // parametros: nome tabela , clausula WHYERE
            let sql = upd('clientes', 'ID = '+ cliente.ID )

            con.query( sql, [  cliente.NOMECLIENTE, cliente.CPFCLIENTE, cliente.CNPJCLIENTE, cliente.FONECLIENTE, cliente.EMAILCLIENTE ], (err, result, fields) => {
            if (err) res.status(204).send(err);

                res.status(200).send({ msg:'Alterado com Sucesso!', type: 'ok'})
            })


        } catch (err) {
            res.status(401).send({ msg:'Erro interno! Já estamos verificando.  ', type: 'err'})    
        }


    }


    const retornarClientes = async (req, res) => {

        // connection 
        const con = app.config.db.connection

        // sql  
        const sql = 'SELECT * FROM clientes;' 
        
        // transaction 
        try {

            con.query(sql, (err, result, fields) => {
                if (err) console.log(err);

                let clients = result

                if (clients) {
                    res.status(200).send(clients)            
                } else  {
                    res.status(205).send('')            
                }
            })
            
        } catch(err) {
            res.status(400).send({ msg: 'Erro interno, tente novamente em alguns instantes...', type:'err'})
        }

    }

    const retornarCliente = async ( req, res ) => {

        // Connection..
        const con = app.config.db.connection

        // sql 
        const sql = 'SELECT * FROM clientes C WHERE C.ID = (?)'


        console.log('body ' + req.body.idCliente)

        // execute 
        con.query(sql, [ req.body.idCliente ], (err, result, fields ) => {

            if (err) console.log('Cliente nao encontrado:  '+err );

            res.status(200).send(result)

        })
    }

    // Cadastrar novo cliente...  
    const Cadastrar = async ( client ) => {

        // função que vai preparar o insert...
        const functionSql = app.config.dic.gerarInsert        
        
        // connection... 
        const con = app.config.db.connection

        // SQL... 
        let sql = await functionSql('clientes')

        // Values 
        let values = [ client.NOMECLIENTE, client.CPFCLIENTE, client.CNPJCLIENTE, client.FONECLIENTE, client.EMAILCLIENTE, client.NOW ]

        con.query( sql , [ values ], (err, res, fields) => {
            
            // guardar log desse erro
            if (err) {
                console.log('Erro ao registrar cliente: '+err);
                return false
            } else {
                console.log('Cliente cadastrado com sucesso!')
            }
        })
    }

    // Validar dados do cadastro do cliente... 
    const validarCliente = ( client ) => {

        const ehVazio =  app.config.validation.ehVazio

        // Recuperar dados         
        if (ehVazio(client.NOMECLIENTE)) {            
            return { msg: 'Informe o nome do Cliente!', type: 'err' }
        } 
        if (ehVazio(client.CPFCLIENTE) && (ehVazio(client.CNPJCLIENTE))) {            
            return { msg: 'Informe o CPF/CNPJ do Cliente!', type: 'err' }
        }
        if (ehVazio(client.FONECLIENTE)) {            
            return { msg: 'Informe o telefone do Cliente!', type: 'err' }            
        }   
        if (ehVazio(client.EMAILCLIENTE)) {            
            return { msg: 'Informe o e-mail do Cliente!', type: 'err' }            
        }
    
        return true
    }

    return { addCliente, retornarClientes, updateCliente, deleteCliente, retornarCliente }
}