/*
 * - @Author: Bruno Henrique
 */

 module.exports = app => {

    const CreateDB = (con) => {

        // SQL
        const sqlCreate = 'CREATE DATABASE IF NOT EXISTS reservas;'
        const sqlUse    = 'USE reservas;'

        // query 
        con.query(sqlCreate, (err, res, fields) => {
            if (err) {
                console.log('Erro ao criar base: '+err);
            } else {
                // dar use na base criada
                con.query(sqlUse, (err, res, fields) => {
                    if (err) console.log('Erro ao selecionar base! ' +err);
                    console.log('Create Database..')      
                    
                    // Aqui vamos seguir com a estruturacao do banco
                    CreateTables(con) 
                    CreateUserMestre(con)                    
                })                      
            }
        })
    }
    
    // CREATE TABLES
    const CreateTables = (con) => {

        // CREATE USER 
        const sqlCreateUser = 'CREATE TABLE IF NOT EXISTS user ('+
                              '   ID        INT AUTO_INCREMENT PRIMARY KEY,'+
                              '   EMAIL     VARCHAR(30) NOT NULL,' +
                              '   NOME      VARCHAR(30) NOT NULL,' +
                              '   PASSWORD  VARCHAR(100) NOT NULL,' +
                              '   NIVEL     INT NOT NULL)';
        
        con.query(sqlCreateUser, (err, res, fields) => {
            if (err) console.log('Falha ao criar tabela user. ERR: '+err)
            console.log('Create table user...');

        })

        // CREATE _AUTH 
        const sqlCreate_auth = 'CREATE TABLE IF NOT EXISTS _auth ('+
                               '    ID      INT AUTO_INCREMENT PRIMARY KEY,'+ 
                               '    EMAIL   VARCHAR(20),'+ 
                               '    TOKEN   VARCHAR(100),'+ 
                               '    IAT     INT, '+
                               '    EXP     INT); '; 
        con.query(sqlCreate_auth, (err, res, fields) => {
            if (err) console.log('Falha ao criar tabela _auth. ERR: '+err)                    
        })

        
        // CREATE CLIENTE 
        const sqlCreateClients = 'CREATE TABLE IF NOT EXISTS clientes ('+ 
                                 '  ID INT AUTO_INCREMENT PRIMARY KEY, ' +
                                 '  NOMECLIENTE  VARCHAR(150), ' + 
                                 '  CPFCLIENTE   VARCHAR(20),' + 
                                 '  CNPJCLIENTE  VARCHAR(20),' + 
                                 '  FONECLIENTE  VARCHAR(20),' + 
                                 '  EMAILCLIENTE VARCHAR(150),' + 
                                 '  DATACADASTRO BIGINT);'


        con.query(sqlCreateClients, (err, res, fields) => {
            if (err) console.log('Erro ao criar tabela: clients. ERR: '+ err);            
        })


        // CREATE RESERVAS 
        const sqlCreateReservas = 'CREATE TABLE IF NOT EXISTS reservas ('+ 
                                 '  ID INT AUTO_INCREMENT PRIMARY KEY, ' +
                                 '  TITULO      VARCHAR(250), ' + 
                                 '  IDCLIENTE   INTEGER,' + 
                                 '  DATAINICIO  DATETIME,' + 
                                 '  DATAFIM     DATETIME,' + 
                                 '  COR         VARCHAR(150))';
                                 


        con.query( sqlCreateReservas, (err, res, fields) => {
            if (err) console.log('Erro ao criar tabela: reservas. ERR: '+ err);            
        })
    } 
    
    const CreateUserMestre = (con) => {

        // Vamos inserir os usuários padroes  
        const sqlInsertUsers = 'INSERT INTO user '+ 
                              ' (ID, EMAIL, NOME, PASSWORD, NIVEL) ' +
                              'VALUES '+
                              ' (1, "hpbruno@outlook.com", "mestre", "28ee56a91c53545da8782a7903709f0d", 10)'; // Guardar a senha criptografada 

        con.query(sqlInsertUsers, (err, res, result) => {
            if (err) console.log('Usuário mestre já existe!');
            console.log('Preparando usuários!');
        })                            
    }

    return { CreateDB }
}