/*
    - A ideia aqui é termos um espelho da base para que possamos gerar os inserts, updates, deletes dinamicamente. 
*/

module.exports = app => {

    // Tabelas
    const tableClients = [

        {
            name: "ID", 
            type: "INTEGER"
        },
        {
            name: "NOMECLIENTE", 
            type: "VARCHAR"
        },{
            name: "CPFCLIENTE", 
            type: "INTEGER"
        
        },{
            name: "CNPJCLIENTE", 
            type: "INTEGER"
        }, 
        {
            name: "FONECLIENTE", 
            type: "INTEGER"
        },{
            name: "EMAILCLIENTE", 
            type: "VARCHAR"

        },{
            name: "DATACADASTRO", 
            type: "INTEGER"
        }
    ]

    const tableReservas = [

        {
            name: "ID", 
            type: "INTEGER"
        },
        {
            name: "TITULO", 
            type: "VARCHAR"
        },{
            name: "IDCLIENTE", 
            type: "INTEGER"
        },{
            name: "DATAINICIO", 
            type: "DATETIME"
        
        },{
            name: "DATAFIM", 
            type: "DATETIME"
        },{
            name: "COR", 
            type: "VARCHAR"
        }
    ]

    const gerarSelect = ( nomeTabela, where, order ) => {
        let sql = 'SELECT * FROM ' + nomeTabela  

        //Tratar Where 
        if ( where != '' ) {
            sql += ' WHERE ' + where
        }

        // Tratar Order 
        if ( order != '' ) {
            sql += ' ORDER BY '+ order + ';'
        }

        return sql
    }

    const gerarUpdate = ( nomeTabela, where ) => {
        let sql = 'UPDATE ' + nomeTabela 
                + ' SET ' 

        switch (nomeTabela) {
            case 'clientes':
                
     
     
                tableClients.forEach( cli => {            
                    if ((cli.name != 'ID') && (cli.name != 'DATACADASTRO')) {
                        sql += cli.name + ' = ?, '
                    }
                })


                //Vamos tratar a virgular que ficou no ultimo campo...  
                sql = sql.substr(0, sql.length -2) 

                sql+= ' WHERE 1=1 AND ' + where
             
            break;
        
            case 'reservas':
                tableReservas.forEach( reserva => {            
                    if (reserva.name != 'ID') {
                        sql += reserva.name + '= ?, '
                    }
                })

                //Vamos tratar a virgular que ficou no ultimo campo...  
                sql = sql.substr(0, sql.length -2) 

                sql += ' WHERE 1=1 AND '+ where
            break;
                default:
                break;
        }
        
        return sql
    }

    // Vamos preparar o insert conforme os parametros...
    const gerarInsert = ( nomeTabela ) => {

        // Cabeçalho da query...
        let sql = 'INSERT INTO '+ nomeTabela+' '
    
        // vamos preparar os campos...
        sql += '('


        switch (nomeTabela) {
            case 'clientes':
                
                tableClients.forEach( cli => {            
                    if (cli.name != 'ID') {
                        sql += cli.name + ','            
                    }
                })
            
            break;
        
            case 'reservas':
                tableReservas.forEach( reserva => {            
                    if (reserva.name != 'ID') {
                        sql += reserva.name + ','            
                    }
                })
            break;


            default:
                break;
        }
   


        //Vamos tratar a virgular que ficou no ultimo campo...  
        sql = sql.substr(0, sql.length -1) 

        // Fechar o parenteses...
        sql += ')'

        // Definir os parametros da query... 
        sql += ' VALUES (?)'

        return sql;
    }

    return { tableClients, gerarInsert, gerarUpdate, gerarSelect }
}