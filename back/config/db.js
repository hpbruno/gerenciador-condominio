/*
 * - @Author: Bruno Henrique
 **/

require('dotenv/config')
// const { db } = require('../.env')
const mysql = require('mysql')

// parameters
const host = process.env.DB_HOST
const port = process.env.DB_PORT
const user = process.env.DB_USER
const password = process.env.DB_PASS
// const host = db.host
// const port = db.port 
// const user = db.user
// const password = db.password


module.exports = app => {


    console.log( host );
    console.log( port );
    console.log( user );
    console.log( password );
    try {
        // Criando connexão
        const connection = mysql.createConnection({
            host, 
            port, 
            user, 
            password
        })

        // Migrations 
        connection.connect(function(err){

            // Err 
            if (err) {
                //return console.log('user: '+user + 'psw: ' +password )
                return console.log('Erro ao conectar ao banco. ERR: '+err )
                
            }

            // Preparando Banco de dados 
            console.log('Preparando banco de dados!') 
            app.migrations.migration0001.CreateDB(connection)        

        })

        return { connection }

    } catch (e) {
        console.log(e.message);
    }

}