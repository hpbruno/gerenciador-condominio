// const { authSecret } = require('../.env')
const  authSecret  = 'f0fp844!@#'
const passport = require('passport')
const passportJwt = require('passport-jwt')
const { Strategy, ExtractJwt} = passportJwt

module.exports = app => {

    // Parametros 
    const params = {
        secretOrKey: authSecret, 
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
    }

    // aqui é definido a estratégia de acesso 
    const strategy = new Strategy(params, (payload, done) => {

        // conection DB 
        const con = app.config.db.connection

        // Vamos tentar localizar o usuário no banco, se ele existir vamos validar o acesso 
        const sql = 'SELECT * FROM user WHERE EMAIL = ?;'
        let values = payload.EMAIL 

        console.log(values);

        con.query(sql, [ values ], (err, res, fields) => {
            if (err) console.log('Erro ao verificar login: '+err);
            
            let user = res 
            user.forEach(usr => {
                try {
                    // Se o usuário existir vamos retornar o payload
                    done(null, usr ? {...payload } : false) 
                } catch (err) {
                    done(err, false)
                }
            })
        })

    })

    passport.use(strategy)

    return { authenticate: () => passport.authenticate('jwt', { session: false }) }

}