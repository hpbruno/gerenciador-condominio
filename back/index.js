/*
 - @Author: Bruno Henrique
*/

require('dotenv/config')

const app = require('express')()
const consign = require('consign')

const port = process.env.PORT

consign()
    .include('./config/passport.js')
    .then('./config/db.js')
    .then('./config/dic.js')
    .then('./config/validation.js')
    .then('./migrations')
    .then('./config/middlewares.js')
    .then('./api')
    .then('./config/routes.js')
    .into(app)


app.listen(port, () => console.log('Backend online. port: '+port))
