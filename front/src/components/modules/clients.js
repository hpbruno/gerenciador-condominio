/*
 * @author: Bruno Henrique 
 */

import { maskCPF, maskCNPJ, maskFone } from '../../../mask.js'
import { ehVazio, createElements, exibirAlert, closeAlert, desabilitarInteracao, formatarData, limparWorkspace, limparModal, droparCampo,  closeModal, exibirConfirm, returnWidth, returnHeight, baseapi, urlapp } from '../../../global.js';


// cliente 
class cliente {

    constructor( nome, cpfcnpj, telefone, email, idCliente ) {
        
        this.nome = nome        
        this.TratarTipoPessoa(cpfcnpj)
        this.telefone = telefone
        this.email = email
        this.idCliente = idCliente
    }

    getNome() {
        return this.nome
    }

    getId() {
        return this.idCliente
    }


    retornarListaCampos(){
        let list = [] 

        list.push(this.nome)
        list.push(this.cpfcnpj)        
        list.push(this.telefone)
        list.push(this.email)

        return list
    }

    retornarJson(){
        
        let json = {
            ID:           this.idCliente,
            NOMECLIENTE:  this.nome,             
            CPFCLIENTE:   this.cpf, 
            CNPJCLIENTE:  this.cnpj, 
            FONECLIENTE:  this.telefone, 
            EMAILCLIENTE: this.email
        }

        return json
    }

    TratarTipoPessoa( CNPJCPF ) {
        
        if (CNPJCPF.lenght > 14) {
            this.cnpj = CNPJCPF         
        } else {
            this.cpf =  CNPJCPF         
        }
    }

    CarregarCliente() {
        
        document.getElementById('id-cliente').setAttribute('idreg', this.idCliente)
        document.getElementById('field-nome').value         = this.nome

        if (this.cpf) {
            document.getElementById('field-cpfcnpj').value  = this.cpf
        } else {
            document.getElementById('field-cpfcnpj').value  = this.cnpj
        }
        
        document.getElementById('field-telefone').value     = this.telefone
        document.getElementById('field-email').value        = this.email
    }


}



// Fixas 
let listaClientes = []
let emEdicao = false

// Aqui tem que ser let pois dependento do tamanho da tela vamos eliminar algumas colunas
 let colunsGrid = []
// {
//     "id"  : "NOMECLIENTE",
//     "name": "Cliente", 
//     "tam" : 20
// },{
//     "id"  : "CPFCNPJCLIENTE",
//     "name": "CPF/CNPJ", 
//     "tam" : 7
// },{
//     "id"  : "FONECLIENTE",
//     "name": "Telefone", 
//     "tam" : 7
// },{
//     "id"  : "EMAILCLIENTE",
//     "name": "Telefone", 
//     "tam" : 7
// },{
//     "id"  : "ACOES",
//     "name": "Ações", 
//     "tam" : 7
// }]

function TratarEventos () {

    // Abrir cadastro cliente
    const btnCadastrarCliente = document.getElementById('btn-cadastrar-cliente')
    btnCadastrarCliente.onclick = clic => {
        configurarModal()
    }

    // procurar 
    const btnProcurar = document.getElementById('btnProcurar')
    btnProcurar.onclick = clic => {
        procurar()
    } 

    const btnAtualizar = document.getElementById('btn-atualizar')
    btnAtualizar.onclick = clic => {
        procurar()
    }
} 

function TratarEventosModal() {
    
    // Salvar cadastro cliente    
    droparCampo( 'btn-salvar-modal' )    
    const btnSalvarCliente = createElements('button', 'btn-salvar-modal', 'btn btn-primary', 'modal-footer', 'type','button')    
    btnSalvarCliente.innerHTML = 'Salvar Cliente'    
    btnSalvarCliente.onclick = clic => {

        SalvarCliente()
    }

    const btnFecharModal = document.getElementById('btn-fechar-modal')
    btnFecharModal.onclick = click => {

        if (( emEdicao ) && ( btnFecharModal.getAttribute('data-dismiss') == '' )) {

            bootbox.confirm({
                message: 'Opa, você não salvou as informações, deseja sair mesmo assim? ',
                buttons: {
                    confirm: {
                        label: 'Sim',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'Não',
                        className: 'btn-danger'
                    }
                },        
                callback:  function (result) {
                    
                    if ( result ) {
                        closeModal()
                    }
                }
            })    

        }
    }

    

    // Mascaras... 
    const txtCPFCNPJ = document.getElementById('field-cpfcnpj') 
    txtCPFCNPJ.onkeypress = key => {
        carregarMascaras()
    }
    const txtFone = document.getElementById('field-telefone')
    txtFone.onkeypress = key => {
        carregarMascaras()
    }

}

function tratarEventosGrid() {
    
    const rows = document.getElementById('row-grid')    
    rows.childNodes.forEach(node => {

        node.childNodes.forEach(action => {

            if (action.id == 'act-delete') {
                action.onclick = clic => {                    
                    
                    /* Isolar Este metodo  */
                    bootbox.confirm({
                        message: 'Deseja realmente excluir o registro?',
                        buttons: {
                            confirm: {
                                label: 'Sim',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'Não',
                                className: 'btn-danger'
                            }
                        },        
                        callback:  function (result) {
                            
                            if ( result ) {
                                deleteCliente(action.getAttribute('idreg'))    
                            }
                        }
                    })    
                }
            }


            if (action.id == 'act-edit') {
                action.onclick = clic => {
                    
                    // Configurar o modal 
                    if ( action.getAttribute('idreg') ) {
                        
                        emEdicao = true
                        
                        configurarModal(action.getAttribute('idreg'))
                    }                    
                }                
            }        
        })
    })
}

function configurarModal( idCliente ) {

        // Limpar o body 
        limparModal()
        const modalTitulo = document.getElementById('modal-title')
        modalTitulo.innerHTML = 'Cadastrar Cliente'

        // Retornar form do modal
        const modalBody = document.getElementById('modal-body')
        modalBody.innerHTML = RetornarForm()

        // Tratar os eventos do modal, mascaras..
        TratarEventosModal()

        retornarCliente( idCliente )
}

function retornarCliente( idCliente ) {

    axios.post(baseapi + 'retornarCliente', { idCliente })
        .then(res => {

            // Preencher modal com os dados do cliente.. 
            let result = res.data
            
            result.forEach(cli => {
            
                // Tratar cpf - cnpj
                let cpfcnpj                
                if (cli.CPFCLIENTE) {
                    cpfcnpj = cli.CPFCLIENTE
                } else {
                    cpfcnpj = cli.CNPJCLIENTE
                }
  
                let client = new cliente( cli.NOMECLIENTE, 
                                          cpfcnpj,
                                          cli.FONECLIENTE, 
                                          cli.EMAILCLIENTE, 
                                          idCliente )

                client.CarregarCliente()

            })
            
        })
}


function procurar() {
    CarregarDados()
}


function deleteCliente( idreg ) {   

    axios.post(baseapi+ 'deleteCliente', { idCliente: idreg })
        .then(res =>{
            exibirAlert( res.data.msg, res.data.type  )
        })
    procurar()
}


function SalvarCliente() {

    let msg 
    let type
    let idCliente = document.getElementById('id-cliente').getAttribute('idreg')   
    
    if (idCliente) {
        
        let cli = new cliente( document.getElementById('field-nome').value,
                               document.getElementById('field-cpfcnpj').value,                          
                               document.getElementById('field-telefone').value,
                               document.getElementById('field-email').value, 
                               idCliente ) 

        // Vamos enviar os dados para o server... 
        axios.post(baseapi + 'updateCliente', cli.retornarJson() )
        .then(res => {        
            
            // Exibir resposta 
            msg  = res.data.msg
            type =  res.data.type    

            // Exibir resposta 
            exibirAlert( msg, type )
                            
            // fechar o modal 
            if (type === 'ok') {
                closeModal()
            }

            // vamos atualizar a lista de clientes 
            procurar()

        })                               

    } else {

        // Carrgar o cliente...
        let cli = new cliente( document.getElementById('field-nome').value,
                               document.getElementById('field-cpfcnpj').value,                          
                               document.getElementById('field-telefone').value,
                               document.getElementById('field-email').value       )   

        // Vamos enviar os dados para o server... 
        axios.post(baseapi + 'cadastrarCliente', cli.retornarJson() )
            .then(res => {        
                
                // Exibir resposta 
                msg  = res.data.msg
                type =  res.data.type    


                // Exibir resposta 
                exibirAlert( msg, type )
                            
                // fechar o modal 
                if (type === 'ok') {
                    closeModal()
                }

                // vamos atualizar a lista de clientes 
                procurar()

            })
    }



}

function CarregarDados() {  

    //limpar a container e a grid         
    const grid = document.getElementById('row-grid')
    if ( grid ) grid.outerHTML = ''

    // Carregar clientess     
    axios.get(baseapi+'retornarClientes')
        .then(res => {

            let data = res.data
            createElements('div','row-grid','row','container-grid','style','width: 90%; margin-left: 5%;')

            // Cabecalho da grid 
            createElements('div', 'colunm-cab', 'col cab', 'row-grid', '', '').innerHTML = 'Nome'
            // createElements('div', 'colunm-cab', 'col cab', 'row-grid', '', '').innerHTML = 'CPF/CNPJ'            
            createElements('div', 'colunm-cab', 'col cab', 'row-grid', '', '').innerHTML = 'Fone'
            // createElements('div', 'colunm-cab', 'col cab', 'row-grid', '', '').innerHTML = 'Email'
            createElements('div', 'colunm-cab', 'col cab', 'row-grid', '', '').innerHTML = 'Ações'
            createElements('div', 'br', 'w-100', 'row-grid', '', '')
            
            data.forEach(cli => {
                
                colunsGrid.forEach(cln => {
                    
                    if (cln.id === 'NOMECLIENTE')    createElements('div', 'colunm-row', 'col row', 'row-grid', '', '').innerHTML = cli.NOMECLIENTE
                    // if (cln.id === 'CPFCNPJCLIENTE') createElements('div', 'colunm-row', 'col row', 'row-grid', '', '').innerHTML = cli.CPFCLIENTE                    
                    if (cln.id === 'FONECLIENTE')    createElements('div', 'colunm-row', 'col row', 'row-grid', '', '').innerHTML = cli.FONECLIENTE
                    // if (cln.id === 'EMAILCLIENTE')   createElements('div', 'colunm-row', 'col row', 'row-grid', '', '').innerHTML = cli.EMAILCLIENTE
                    if (cln.id === 'ACOES') {                    
                        createElements('div', 'colunm-row '+cli.ID,'col row','row-grid','style','justify-content: center;')
                        createElements('img', 'act-edit','act-edit','colunm-row '+cli.ID,'style,src,idreg,data-toggle,data-target','height:25px;,/assets/icons/edit_cli.png,'+cli.ID+',modal,#modal')
                        createElements('img', 'act-delete','act-delete','colunm-row '+cli.ID,'style,src,idreg','height:25px;,/assets/icons/delete_cli.png,'+cli.ID)
                      
                    }
                    
                })
                
                // quebra de linha 
                createElements('div', 'br', 'w-100', 'row-grid', '', '')
            })
            
            tratarEventosGrid()

        })

}

function RetornarForm() {
    
    const html = '<!-- Form -->'+
                '<form ac>'+    
                '    <!-- Nome cliente -->'+
                '    <label id="id-cliente"></label>' +
                '    <div class="form-group">'+
                '      <label for="exampleFormControlInput1">Nome</label>'+
                '      <input type="text" class="form-control" id="field-nome" placeholder="">'+
                '    </div>                    '+
             '    <!-- cpf/cnpj cliente -->'+
                '    <div class="form-group">'+
                '        <label for="exampleFormControlInput1">CPF/CNPJ</label>'+
                '        <input type="text" class="form-control" id="field-cpfcnpj" placeholder="999.999.999-99">'+
                '    </div>                  '+
             '    <!-- Telefone -->'+
                '    <div class="form-group">'+
                '        <label for="exampleFormControlInput1">Telefone </label>'+
                '        <input type="text" class="form-control" id="field-telefone" placeholder="(99) 99999-9999">'+
                '    </div>           '+
             '    <!-- Email -->'+
                '    <div class="form-group">'+
                '        <label for="exampleFormControlInput1">Email</label>'+
                '        <input type="email" class="form-control" id="field-email" placeholder="">'+
                '    </div>           '+
                '</form>';
     return html                    
    
}
    
function carregarMascaras() {

    // CPF - CNPJ
    let txtCPFCNPJ = document.getElementById('field-cpfcnpj')
    
    if ((txtCPFCNPJ.value.length >= 0) && (txtCPFCNPJ.value.length <= 14)) {        
        txtCPFCNPJ.value = maskCPF(txtCPFCNPJ.value)               
    } else {
        txtCPFCNPJ.value = maskCNPJ(txtCPFCNPJ.value)               
    }    

    // Telefone 
    let txtFone = document.getElementById('field-telefone')
    txtFone.value = maskFone(txtFone.value)
}


function ConfigurarViewCliente() {
    
    // Limpar container..    
    limparWorkspace()

    // Criar os elementos fixos da tela..
    CriarElementos()

    tratarRedimensionamentoClientes()
    // Vamos carregar os clientes e tratar os eventos
    // CarregarDados() 
    // TratarEventos()    
  
}

function CriarElementos() {

    // Grid...
    createElements('div','container-grid','container','container-workspace','','')    

    // Adicionar cliente                                   
    const addCliente = createElements('li', 'nav-acoes-item', 'nav-item sub-navitem', 'nav-acoes', '','')
    const actAddCliente = createElements('a', 'btn-cadastrar-cliente', 'nav-link', 'nav-acoes-item', 'data-toggle,data-target','modal,#modal')
    actAddCliente.innerHTML = 'Adicionar Cliente'   
    addCliente.appendChild(actAddCliente)
    

    // Atualizar
    const btnAtualizar = createElements('li', 'nav-acoes-item', 'nav-item sub-navitem', 'nav-acoes', '','')
    const actAtualizar = createElements('a', 'btn-atualizar', 'nav-link', 'nav-acoes-item', '','')
    actAtualizar.innerHTML = 'Atualizar'
    btnAtualizar.appendChild(actAtualizar)

}

/* REVER */
function tratarRedimensionamentoClientes() {


    // -- TRATAR GRID -- //
    // -> tamanho das grids em percentual

    // Maior que W: 1410px... 
    // if (window.innerWidth > 1410) {
    //     colunsGrid = [{
    //         "id"  : "NOMECLIENTE",
    //         "name": "Cliente", 
    //         "tam" : 20
    //     },{
    //         "id"  : "EMAILCLIENTE",
    //         "name": "E-mail", 
    //         "tam" : 20
    //     },{
    //         "id"  : "CPFCNPJCLIENTE",
    //         "name": "CPF/CNPJ", 
    //         "tam" : 10
    //     },{
    //         "id"  : "FONECLIENTE",
    //         "name": "Telefone", 
    //         "tam" : 10
    //     },{
    //         "id"  : "DATACADASTRO",
    //         "name": "Cadastrado em", 
    //         "tam" : 15
    //     },{
    //         "id"  : "ACOES", 
    //         "name": "Açoes", 
    //         "tam" : 10
    //     }]       
    // }

    // // Até W: 1410px...     
    // if (window.innerWidth <= 1410) {
    //     colunsGrid = [{
    //         "id"  : "NOMECLIENTE",
    //         "name": "Cliente", 
    //         "tam" : 25
    //     },{
    //         "id"  : "EMAILCLIENTE",
    //         "name": "E-mail", 
    //         "tam" : 20
    //     },{
    //         "id"  : "CPFCNPJCLIENTE",
    //         "name": "CPF/CNPJ", 
    //         "tam" : 15
    //     },{
    //         "id"  : "FONECLIENTE",
    //         "name": "Telefone", 
    //         "tam" : 15
    //     },{
    //         "id"  : "ACOES", 
    //         "name": "Açoes", 
    //         "tam" : 10
    //     }]        
    // }

    // // Até W: 1080px...     
    // if (window.innerWidth <= 1080) {
    //     colunsGrid = [{
    //         "id"  : "NOMECLIENTE",
    //         "name": "Cliente", 
    //         "tam" : 30
    //     },{
    //         "id"  : "EMAILCLIENTE",
    //         "name": "E-mail", 
    //         "tam" : 25
    //     },{
    //         "id"  : "FONECLIENTE",
    //         "name": "Telefone", 
    //         "tam" : 20
    //     },{
    //         "id"  : "ACOES", 
    //         "name": "Açoes", 
    //         "tam" : 10
    //     }]        
    // }

    // // Até W: 880px...     
    // if (window.innerWidth <= 880) {
    //     colunsGrid = [{
    //         "id"  : "NOMECLIENTE",
    //         "name": "Cliente", 
    //         "tam" : 30
    //     },{
    //         "id"  : "EMAILCLIENTE",
    //         "name": "E-mail", 
    //         "tam" : 30
    //     },{
    //         "id"  : "FONECLIENTE",
    //         "name": "Telefone", 
    //         "tam" : 25
    //     }]        
    // }


    // -- MOBILE -- //
    // Até W: 375px...     
    if (returnWidth() <= 375) {
        colunsGrid = [{
            "id"  : "NOMECLIENTE",
            "name": "Cliente", 
            "tam" : 45
        },{
            "id"  : "FONECLIENTE",
            "name": "Telefone", 
            "tam" : 35
        },{
            "id"  : "ACOES",
            "name": "Ações", 
            "tam" : 7
        }]        
    }

    // Até W: 320px...     
    if (window.innerWidth <= 320) {
        colunsGrid = [{
            "id"  : "NOMECLIENTE",
            "name": "Cliente", 
            "tam" : 25
        },{
            "id"  : "FONECLIENTE",
            "name": "Telefone", 
            "tam" : 15
        }]        
    }

    CarregarDados() 
    TratarEventos()        
}

export { TratarEventos, TratarEventosModal, procurar, SalvarCliente, CarregarDados, RetornarForm, ConfigurarViewCliente}