/*
 * @author: Bruno Henrique 
 */

import { maskCPF, maskCNPJ, maskFone } from '../../../mask.js'
import { ehVazio, createElements, desabilitarInteracao, formatarData, createInputDateBootstrap, limparWorkspace, limparModal, droparCampo, exibirAlert, closeModal, retornartoken, returnHeight, returnWidth, baseapi, urlapp } from '../../../global.js';


class reservas {

    constructor( id, titulo, idCliente, dateinicio, datefim,  cortag ) {
        this.id          = id
        this.titulo      = titulo
        this.idCliente   = idCliente
        this.dateinicio  = dateinicio
        this.datefim     = datefim
        this.cortag      = cortag
    }

    retornarJson() {

        let json = {
            ID:  this.id,
            TITULO: this.titulo, 
            IDCLIENTE: this.idCliente, 
            DATEINICIO: this.dateinicio, 
            DATEFIM: this.datefim, 
            CORTAG: this.cortag
        }   

        return json
    }
}

// Fixas 
let emEdicao = false
// Array de dias 
let dias = ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabado']

// // Por padrão serao 7 dias, porem ira mudar conforme a resolucao
// let colunsGrid = [{
//     "id"  : "",
//     "name": "Cliente", 
//     "tam" : 20
// },{
//     "id"  : "CPFCNPJCLIENTE",
//     "name": "CPF/CNPJ", 
//     "tam" : 7
// },{
//     "id"  : "FONECLIENTE",
//     "name": "Telefone", 
//     "tam" : 7
// },{
//     "id"  : "EMAILCLIENTE",
//     "name": "Telefone", 
//     "tam" : 7
// },{
//     "id"  : "ACOES",
//     "name": "Ações", 
//     "tam" : 7
// }]

function ConfigurarViewReservas() {

    // Vamos limpar o container..
    limparWorkspace()

    // Criar elementos fixos da tela...
    CriarElementos()

    // Criar filtros  
    CriarFiltros()

    // Eventos
    TratarEventos()

    // Atualizar informacoes
    AtualizaPanel()
}

function CriarFiltros(){
    
    // Filtrar Datas 
    const divData = createElements('div', 'div-data', 'div-data', 'container-filter', '','')
    
    createInputDateBootstrap('data-inicial', 'div-data', 'Inicio')
    createInputDateBootstrap('data-final',   'div-data', 'Final')

    const btnFiltrar = createElements('button', 'btn-filtrar', 'btn-filtrar', 'container-filter' , 'type,class', 'button,btn btn-dark' )
    btnFiltrar.innerHTML = 'Filtrar' 


}

function CriarElementos(){

    // Grid
    createElements('div','container-grid','container','container-workspace','','')    

    // Datas
    createElements('div','container-filter','container-filter','container-grid','','')    

    // Row
    createElements('div','row-grid','row','container-grid','style','width: 90%; margin-left:5%; border:none;')    

    // Criar colunas conforme o dia atual...
    configurarGridConformeSemana( )

    // Adicionar Reserva 
    const addReserva = createElements('li', 'nav-acoes-item', 'nav-item sub-navitem', 'nav-acoes', '','')
    const actAddreserva = createElements('a', 'btn-adicionar-reserva', 'nav-link', 'nav-acoes-item', 'data-toggle,data-target','modal,#modal')
    actAddreserva.innerHTML = 'Nova Reserva'   
    addReserva.appendChild(actAddreserva)
    
    // Atualizar
    const btnAtualizar = createElements('li', 'nav-acoes-item', 'nav-item sub-navitem', 'nav-acoes', '','')
    const actAtualizar = createElements('a', 'btn-atualizar', 'nav-link', 'nav-acoes-item', '','')
    actAtualizar.innerHTML = 'Atualizar'
    btnAtualizar.appendChild(actAtualizar)

}

function TratarEventos() {

    // btn adiciona reserva 
    const btnAddReserva = document.getElementById('btn-adicionar-reserva')
    btnAddReserva.onclick = clic => {

        configurarModal();

    }
    
    // Atualizar 
    const btnAtualizar = document.getElementById('btn-atualizar')
    btnAtualizar.onclick = () => {
        AtualizaPanel()
    }    

    // Filtrar 
    const btnFiltrar  = document.getElementById('btn-filtrar')
    btnFiltrar.onclick = () => {

        AtualizaPanel()
    }
}

function configurarModal( idReserva ) {

         // Limpar o modal  
         limparModal()

         // Titulo do modal 
         const modalTitulo = document.getElementById('modal-title')
         modalTitulo.innerHTML = 'Nova reserva'
 
         // Retornar form do modal 
         const modalBody = document.getElementById('modal-body')
         modalBody.innerHTML = RetornarForm()
 
         // Tratar os eventos do modal
         TratarEventosModal()
 
         // Carregar Informacoes Modal  
         CarregarClientes()

         // Carregar a reserva conforme o id
         CarregarReserva( idReserva )
}

function CarregarReserva( idReserva ) {

    let id = {
        ID: idReserva
    }

    axios.post(baseapi + 'retornarReserva' ,  id )
        .then(res => {

           
            // Recebemos o retorno do servidor
            let reserva = res.data

            reserva.forEach(rsv => {

                // Alimentar os campos do modal 
                document.getElementById('id-reserva').setAttribute('idreg', rsv.ID)
                document.getElementById('txt-titulo-reserva').value = rsv.TITULO
                document.getElementById('select-cliente').value     = parseInt(rsv.IDCLIENTE)
                document.getElementById('date-inicio').value        = formatarData( 'yyyy-mm-dd hh:mm', rsv.DATAINICIO )
                document.getElementById('date-fim').value           = formatarData( 'yyyy-mm-dd hh:mm', rsv.DATAFIM ) 
                document.getElementById('clr-cortag').value         = rsv.CORTAG 
            })            
        
        // Entrou em Edicao
        emEdicao = true
    })

}

function TratarEventosGrid() {

    const grid = document.getElementById('container-grid')
    grid.childNodes.forEach(node => {
        
        // 
        node.childNodes.forEach(child => {
            
            // Vamos pegar somente os containes das tags
            if ( child.id.includes('row-col-tags') ) {                
                child.childNodes.forEach(tag => {                
                    tag.onclick = () => {
                        configurarModal( tag.getAttribute('idreg') );
                    }
                })
            }
        })        
    })
} 

function CarregarClientes() {

    axios.get(baseapi + 'retornarClientes')
    .then(res => {

        let clientes = res.data 

        clientes.forEach(cli => {

            console.log(cli.NOMECLIENTE);
            createElements('option', 'option-cliente', 'option-cliente', 'select-cliente', 'value', cli.ID.toString()).innerHTML = cli.NOMECLIENTE
            
        })        
    })
}

function limparView() {

    for( let i =0; i <= dias.length; i++) {
        if (document.getElementById('row-col-tags '+ i )) {
            document.getElementById('row-col-tags '+ i ).innerHTML = ''
        }            
    }

}

function AtualizaPanel() {

    // Carregar o token de acesso antes de tudo
    // axios.headers.common['Authorization'] = `bearer ${retornartoken()}`    

    // Clear
    limparView()

    // Datas 
    let datainicial = document.getElementById('data-inicial')
    let datafinal   = document.getElementById('data-final')
    
    let filtro = {
        datainicial: datainicial.value, 
        datafinal  : datafinal.value
    }   

    axios.post(baseapi+'retornarReservas', filtro)
        .then(res => {

            let data = res.data
            
            data.forEach(reserva  => {                
                CriarTagReserva(reserva.ID, reserva.TITULO, reserva.DATAINICIO, reserva.DATAFIM, reserva.COR)
            });

            // Definir os eventos da grid
            TratarEventosGrid()

        })
}

function TratarEventosModal() {    
    
    // Salvar cadastro reserva
    droparCampo( 'btn-salvar-modal' )    
    const btnSalvarReserva = createElements('button', 'btn-salvar-modal', 'btn btn-primary', 'modal-footer', 'type','button')    
    btnSalvarReserva.innerHTML = 'Salvar Reserva'
    btnSalvarReserva.onclick = clic => {       
        SalvarReserva()            
    }

    // Fechar 
    
    document.getElementById('btn-fechar-modal').onclick = () => {        
        closeModal()                
    }
}

function SalvarReserva() {

    let idReserva = document.getElementById('id-reserva').getAttribute('idreg')

    let reserva = new reservas(idReserva,
                               document.getElementById('txt-titulo-reserva').value,
                               document.getElementById('select-cliente').value,
                               document.getElementById('date-inicio').value,
                               document.getElementById('date-fim').value,
                               document.getElementById('clr-cortag').value)
                                
    if ( idReserva ) {                     
        axios.post(baseapi +'updateReservas', reserva.retornarJson() )                               
        .then(res => {
            
            if ( res.data.type == 'ok') {
                // Fechar o modal0
                closeModal()
                
                // Atualizar a grid..
                AtualizaPanel()
            }

            // Informar a resposta do servidor            
            exibirAlert(res.data.msg, res.data.type)

        })  
    } else {
        axios.post(baseapi +'cadastrarReserva', reserva.retornarJson() )                               
            .then(res => {
                
                if ( res.data.type == 'ok') {
                    // Fechar o modal0
                    closeModal()
                    
                    // Atualizar a grid..
                    AtualizaPanel()
                }

                // Informar a resposta do servidor
                exibirAlert(res.data.msg, res.data.type)
            })  
    }

}   

 function CriarTagReserva( idtag, titulo, dateInicio, dateFim, cortag) {


    // tag.. 
    const tag = createElements('div', 'tag-reserva '+idtag, 'tag-reserva', 'row-col-tags ' + retornarDiaSemana(dateInicio) , 'style,data-target,data-toggle', 'background-color:'+cortag+';,#modal,modal') 
    
    // Setar id 
    tag.setAttribute('idreg', idtag)

    // titulo..    
    const tagTitulo = createElements('label', 'lbl-titulo-reserva', 'lbl-titulo-reserva', 'tag-reserva '+idtag, '', '') 
    tagTitulo.innerHTML = titulo

    // Horarios.. 
    createElements('div', 'tag-horarios '+ idtag, 'tag-horarios', 'tag-reserva '+idtag, 'style', 'display: inline-box;') 
    const inicio = createElements('label', 'lbl-reserva lblHorani',  'lbl-reserva lblHorani',  'tag-horarios '+ idtag, '', '')             
    const fim    = createElements('label', 'lbl-reserva lblHorafim', 'lbl-reserva lblHorafim', 'tag-horarios '+ idtag, '', '') 

    inicio.innerHTML = 'Inicio: ' + formatarData('dd/mm/yyyy hh:mm', dateInicio)
    fim.innerHTML    = 'Fim: '    + formatarData('dd/mm/yyyy hh:mm', dateFim)

    // Tratar luminosidade das labels 
    tratarLuminosidade( tagTitulo, tag )
    tratarLuminosidade( inicio, tag )
    tratarLuminosidade( fim, tag )

    retornarDiaSemana(dateInicio)

}

function tratarLuminosidade( label, elemento ) {

    // vamos recuperar a cor do objeto. A cor tem que estar em hexadecimal...
    let color = elemento.style.backgroundColor    
    
    // Vamos tratar aco do elemento para separar depois...
    color = color.replace('rgb(', '')
    color = color.replace(')', '')
    color = color.replaceAll(',', '')
    color = color.split(' ')
    
    // Pegamos o valor de cada cor...
    let r = parseInt( color[0] )
    let g = parseInt( color[1] )
    let b = parseInt( color[2] )

    // Formular para calcular a luminosidade...
    let luminosidade = ( r * 0.2126 ) + ( g * 0.7152 ) + ( b * 0.0722 ) 
    
    // Vamos definir a cor de acordo com a luminosidade
    if (luminosidade >= 128) {
        label.style.color = 'gray'
    } else {
        label.style.color = 'whitesmoke'
    }
}

function retornarDiaSemana( date ) {

    let newDate = new Date( date )
    
    return newDate.getDay()
}

function RetornarForm() {

    const html = '<form>' +
                  '  <div class="row">' +
                  '      <label id="id-reserva"></label> '+
                  '      <div class="form-group row" style="display: inline-block; padding-left: 5%; border: none; " >'+
                  '         <div class="col-10">' +
                  '             <input id="txt-titulo-reserva" type="text" class="form-control"  style="width:75%" placeholder="Titulo">' +
                  '         </div>' +                                                      
                  '         <label for="example-time-input" class="col-2 col-form-label">Cliente</label>'+                                            
                  '         <div class="col">'+
                  '          <select id="select-cliente" class="custom-select" id="inputGroupSelect01">'+
                  '            <option  selected>Selecione...</option>'+                
                  '          </select>'+                  
                  '         </div>'+
                  '         <label for="example-time-input" class="col-2 col-form-label">Inicio</label>'+                          
                  '         <div class="col">'+
                  '             <input id="date-inicio" class="form-control" type="datetime-local"  id="example-datetime-local-input">'+
                  '         </div>'+      
                  '         <label for="example-time-input" class="col-2 col-form-label">Final</label>'+                          
                  '         <div class="col">'+
                  '             <input id="date-fim" class="form-control" type="datetime-local" value="2021-08-19T13:45:00" id="example-datetime-local-input">'+
                  '         </div>'+                      
                  '         <label for="example-color-input" class="col-2 col-form-label">Cor</label>'+           
                  '         <div class="col-10">'+
                  '             <input id="clr-cortag" class="form-control" type="color" value="#563d7c" id="example-color-input">'+
                  '         </div>'+
                  '      </div>'+
                  '  </div>' +
                  '</form>'

    return html

}

function configurarGridConformeSemana() {
    
    // Vamos instanciar a data atual... 
    let date = new Date()         
    let firstDay = date.getDay()    

    // Vamos mostrar 6 dias a frente..
    date.setDate(date.getDate() +5 )  
    let lastDay = date.getDay()

    
    if ( returnWidth() < 400 ) {    
        // Cabecalho
        for (let i = 0;i <= 6; i++) {        
            createElements('div', 'row-col', 'col-sm cab', 'row-grid', '', '').innerHTML = dias[i]
            createElements('div', 'row-col-tags '+i, 'col-sm timeline', 'row-grid', 'style', 'margin-left: 10px;  padding: 0 5px 0px 2px;  background-color: whitesmoke;')
        } 
    } else if ( returnWidth() > 400 ) {    
        // Cabecalho
        for (let i = 0;i <= 6; i++) {        
            createElements('div', 'row-col', 'col-sm cab', 'row-grid', '', '').innerHTML = dias[i]      
        } 
        
        // Quebra linha
        createElements('div', 'br', 'w-100', 'row-grid', '', '')

        // Base
        for (let i = 0;i <= 6; i++) {        
            createElements('div', 'row-col-tags '+i, 'col-sm timeline', 'row-grid', 'style', 'margin-left: 10px;  padding: 0 5px 0px 2px;  background-color: whitesmoke;')
        } 
    }

}

export { ConfigurarViewReservas }