/*
-Author: @Bruno Henriques
*/

import { ehVazio, createElements, exibirAlert, desabilitarInteracao, baseapi, urlapp } from '../../../global.js';

function DefinirElementos() {

    // alert 
    const alert = createElements('div', 'alert', 'alert', '', '', '')

    // Container de login 
    const divLogin = createElements('div', 'container-login', 'container-login', '', '', '')

    // Fundo 
    const divFundo = createElements('div', 'fundo', 'fundo', '', '', '')    

    // logo 
    //const logo = createElements('span', 'logo', 'logo', 'container-login', '', '')
    
    const credenciais = createElements('div', 'credenciais', 'credenciais', 'container-login', '', '')

    // email 
    const email = createElements('input', 'email', 'email', 'credenciais', '', '')
    email.setAttribute('placeholder', 'Informe seu E-mail!')

    // password
    const password = createElements('input', 'password', 'password', 'credenciais', 'type', 'password')
    password.setAttribute('placeholder', 'Informe sua Senha!')

    // Entrar
    const btnentrar = createElements('button', 'btnEntrar', 'btnEntrar', 'credenciais', '', '')
    btnentrar.innerHTML = 'Entrar' 

    // Cadastrar 
    const lnkCadastrar = createElements('link', 'lnkRegistrar', 'lnkRegistrar', 'credenciais', '', '' )
    lnkCadastrar.innerHTML = 'Faça seu Cadastro!'

}

function Validation() {

    // campos 
    let email = document.getElementById('email').value
    let password  = document.getElementById('password').value

    // vamos validar se os valores sao vazios 
    if (ehVazio(email)) {
        exibirAlert('Informe um e-mail!', 'err'); return false;
    }
    if (ehVazio(password)) {
        exibirAlert('Informe a senha!', 'err');   return false;
    }

    return true;
} 

function DefinirEventos() {

    // Entrar 
    const btnEntrar = document.getElementById('btnEntrar')
    btnEntrar.onclick = () => {
        
        // Vamos verificar os valores
        if(Validation()) { 
            ValidarLogin();            
        }
    }

    // Registrar 
    const lnkRegistrar = document.getElementById('lnkRegistrar')
    lnkRegistrar.onclick = () => {
           // Vamos verificar os valores
           if(Validation()) {             
            registrarUsuario();            
        }
    }
}

function registrarUsuario() {

        // Fields
        let email = document.getElementById('email').value
        let password  = document.getElementById('password').value
    
        // Json 
        let login = {
            email, 
            password
        }
    
        axios.post(baseapi + 'register', login) 
             .then(res => {
                if(res.status === 200) {
                    exibirAlert('Usuário registrado com sucesso!', 'ok');  
                }
        })

}

function ValidarLogin() {
    
    // Fields
    let email = document.getElementById('email').value
    let password  = document.getElementById('password').value

    // Json 
    let login = {
        email, 
        password
    }

    axios.post(baseapi + 'login', login) 
         .then(res => {

            if(res.status === 200) {
                console.log(res.data);
                window.location.href = urlapp + `home?token=${res.data.token}`
            } else {
                exibirAlert(res.data , 'ok')
            }

         })
}

function start() {
    /* CUIDADO COM A ORDEM */
    DefinirElementos()
    DefinirEventos()
}

start()