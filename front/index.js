/*
 -Author: @Bruno Henrique
*/

// express 
const express = require('express')
const app = express()

//body parser 
const bodyparser = require('body-parser')
app.use(bodyparser.urlencoded({ extended: true}))

// express files 
app.use(express.static('./src'))
app.use(express.static('./public'))
app.use(express.static('./src/assets')) // favicon

// route login 
app.get('/', (req, res) => { 
    res.sendFile('/login.html', {root: __dirname + '/src/components/login/' })    
})

// route home 
app.get('/home', (req, res) => {
    res.sendFile('/home.html', {root:__dirname + '/src/components/home/'})
})


app.listen(3000, () => { console.log('front rodando na porta 3000') })
