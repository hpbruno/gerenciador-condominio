/*
 * @Author: Bruno Henrique
 */

// URL API dev: 'http://localhost:8083/'
// URL API Prod: 'http://143.110.238.179:8083/'
const baseapi = 'http://143.110.238.179:8083/'

// URL app dev: http://localhost:3000/
// URL app Prod: http://143.110.238.179:3000/
const urlapp = 'http://143.110.238.179:3000/' 


const ehVazio = (field) => {    
    if (field == '') return true
    
    return false
}

const desabilitarInteracao = (container) => {

    /*
     * A ideia aqui é desabilitar todos eventos possiveis do container que recebermos como parametro até que o servidor responda
    */
   
    // Informar o usuário
    exibirAlert('Aguarde...', 'wait') ////

    // Vamos recuperar os filhos     
    const childs = container.children 
    
    // aqui vamos varrer campo por campo e desabilitar
    for (let i = 0; i < childs.length; i++) {
        
        console.log(childs[i]);
        childs[i].setAttribute('disabled', 'true')

    }    
}

function retornartoken(){

    let link = location.href
    if (link.indexOf('token') > 0) {
        let token = link.substring(link.indexOf('=') +1)         
        return token
    }
}

const closeModal = () =>{
    
    const close = document.getElementById('btn-fechar-modal')    
    close.setAttribute('data-dismiss', 'modal')  
    close.click()
    
}

const droparCampo = ( idElement ) => {
    if ( document.getElementById(idElement) ) {
        document.getElementById(idElement).outerHTML = ''
    }
}

const closeAlert = () => {

    document.getElementById('alert').innerHTML = ''
    document.getElementById('alert').style.zIndex = -1
    document.getElementById('alert').style.opacity = 0
} 

const exibirAlert = (msg, tipo) => {
    
    let titulo 
    let color     

    if (tipo == 'err') {       
        titulo = 'Ops, parece que algo deu errado!'  
        color =  '#F8D7DA'
    } else if ( tipo == 'ok' ) {
        titulo = 'Tudo certo!'  
        color  = '#D4EDDA'
    } else if ( tipo == 'wait' ) {
        titulo = 'Aguarde um instante!'  
        color  = '#D4EDDA'
    } else if ( tipo == 'confirm') {
        titulo = 'Tem certeza que deseja realizar esta operação'  
        color  = '#CCE5FF'
    }

    if (msg) { 
        bootbox.alert({
            'title': titulo, 
            'message': msg        
        }).find('.modal-content').css({
            'background-color': color, 
            'border-color': 'transparent'      
        });
    }     
    
}

function exibirConfirm( msg ) {
    let res 

    bootbox.confirm({
        message: msg,
        buttons: {
            confirm: {
                label: 'Sim',
                className: 'btn-success'
            },
            cancel: {
                label: 'Não',
                className: 'btn-danger'
            }
        },        
        callback:  function (result) {
            
            res = result
            
            return result
        }
    })    
}

function createElements(tagName, idTag, className, tagPaiId, attributeName, attributeValue) {

    // Criando elemento 
    const element = document.createElement(tagName)

    // Atribuir o id ao elemento 
    element.setAttribute('id', idTag)

    // vamos attribuir o nome da classe 
    element.className = className 

    // Se existir o tagPaiName, temos que posicionar o elemento abaixo do pai.
    // Do contrário, vamos adicionar ao body..
    if (tagPaiId) {
        document.getElementById(tagPaiId).appendChild(element)
    } else {
        document.body.appendChild(element)    
    }

    // Tratar attributos     
    const separator = ',' // Separador ->> ","
    let listAttributes = attributeName.split(separator)
    let listAttributesValues = attributeValue.split(separator)


    for (let i = 0; i < listAttributes.length; i++) {
        if ((listAttributes[i]) && (listAttributesValues[i])) {

            element.setAttribute(listAttributes[i], listAttributesValues[i])
        }
    }


    return element
}

function limparModal( ) {    
    document.getElementById('modal-body').innerHTML = ''

}

function limparWorkspace() {
    
    // Workspace...
    if(document.getElementById('container-workspace')) {
        document.getElementById('container-workspace').innerHTML = ''            
    }

    // menu acoes
    if (document.getElementById('nav-acoes')) {
        document.getElementById('nav-acoes').innerHTML = ''
    }

}

function formatarData(format, date) {

    // formatar data para segundos | REVER : a data aqui precisa chegar em milisegundos 
    let secDate = new Date(date)

    // Ajustar formato mes
    let mes = (secDate.getMonth() +1)
    if (mes < 10) mes = '0'+mes 

    // Ajustar formato dia 
    let dia = secDate.getUTCDate()
    if (dia < 10) dia = '0'+dia

    // date 
    if (format === 'dd/mm/yyyy') {
        //     // Dia do mês...             // retorna:(0-11) + 1 temos os 12 meses   // Ano... 
        return  dia + '/' + mes       + '/' +     secDate.getFullYear()
    }

    // date and time 
    if ( format === 'dd/mm/yyyy hh:mm' ) {
        return dia + '/' + mes + '/' + secDate.getFullYear() + ' ' + secDate.getHours()+':'+secDate.getMinutes()
    }

    // DAte and time conforme inputs
    console.log(format);
    if ( format === 'yyyy-mm-dd hh:mm' ) {
        return secDate.getFullYear() + '-' + mes + '-' + dia + 'T' + secDate.getHours()+':'+secDate.getMinutes()
    }

}

function returnWidth() {
    return window.innerWidth
}

function returnHeight() {
    return window.innerHeight
}

function createInputDateBootstrap( id, idPai, label ) {

    const divBase = createElements('div', 'div-base-data '+id, 'div-base-data', idPai, 'style', 'display:inline-block;')
    const lblData = createElements('label', 'lblDate', 'lblDate', 'div-base-data '+id, 'for,class', 'example-time-input,col-2 col-form-label')    
    lblData.innerHTML = label

    const div = createElements('div', 'input-data '+id, 'input-data' , 'div-base-data '+id, 'class', 'col')
    const inpData = createElements('input', id, id, 'input-data '+id, 'class,type', 'form-control,datetime-local')

} 

export { ehVazio, createElements, exibirAlert, closeAlert, desabilitarInteracao, formatarData, createInputDateBootstrap, limparWorkspace, limparModal, droparCampo, closeModal, exibirConfirm, retornartoken, returnHeight, returnWidth, baseapi, urlapp }